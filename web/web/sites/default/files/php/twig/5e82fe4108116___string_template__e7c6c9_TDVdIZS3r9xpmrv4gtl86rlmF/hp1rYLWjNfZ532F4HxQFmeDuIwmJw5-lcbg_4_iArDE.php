<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__e7c6c90623aefdfa5cca1ed899d41cba2865f278a0f76f092ce24e0a413c575b */
class __TwigTemplate_e2da7bcd3c943232f46c3fedcb21d26611cca6b35263996c958c725125a6028e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "Orange Studio offers a wide range of web development services which are tailored to each clients individual needs and requirements. We specialize in offering complete end to end solutions in which we take care of the project from start to finish and have a range of maintenance contracts available. Orange Studio is also able to offer Search Engine Optimization and Internet marketing services for your completed website.

We select the right technologies and tools that provide the most flexible and powerful solutions for the situation. We are inspired by the best of today's technology and can apply it to your problems.";
    }

    public function getTemplateName()
    {
        return "__string_template__e7c6c90623aefdfa5cca1ed899d41cba2865f278a0f76f092ce24e0a413c575b";
    }

    public function getDebugInfo()
    {
        return array (  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# inline_template_start #}Orange Studio offers a wide range of web development services which are tailored to each clients individual needs and requirements. We specialize in offering complete end to end solutions in which we take care of the project from start to finish and have a range of maintenance contracts available. Orange Studio is also able to offer Search Engine Optimization and Internet marketing services for your completed website.

We select the right technologies and tools that provide the most flexible and powerful solutions for the situation. We are inspired by the best of today's technology and can apply it to your problems.", "__string_template__e7c6c90623aefdfa5cca1ed899d41cba2865f278a0f76f092ce24e0a413c575b", "");
    }
}
