<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/orange/templates/contact/block--views-block--contact-block-1.html.twig */
class __TwigTemplate_3b436650a6dcdd013c12713a2cdc30c34b4a5d2ebcfb01904bb65e9a1b978c74 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 3, "if" => 12, "block" => 17];
        $filters = ["clean_class" => 5, "escape" => 10];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
";
        // line 3
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 5
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 6
($context["plugin_id"] ?? null)))), 3 => "clearfix"];
        // line 10
        echo "<section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
  ";
        // line 12
        if (($context["label"] ?? null)) {
            // line 13
            echo "    <h2";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title_attributes"] ?? null), "addClass", [0 => "block-title"], "method")), "html", null, true);
            echo ">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "</h2>
  ";
        }
        // line 15
        echo "  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

  ";
        // line 17
        $this->displayBlock('content', $context, $blocks);
        // line 85
        echo "</section>

";
    }

    // line 17
    public function block_content($context, array $blocks = [])
    {
        // line 18
        echo "        <div class=\"contact-us\">
          <div class=\"container\">

              <div class=\"contact-info\">
                <div class=\"row\">
                  <div class=\"col-md-6 col-xs-12\">
                      <div class=\"left\">
                          <h2>Contact Us</h2>
                          <p>
                             <b> Orange Studio</b> <br>
                                  Road 8, Block D, Cairo Free Zone, Nasr City, Cairo, EGYPT.
                          </p>
                      </div>
                  </div>
                  <div class=\"col-md-6 col-xs-12\">
                      <div class=\"right\">
                          <p>
                                      Tel .:  +202-22713519<br>
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                          +202-22713517<br>
                                  Fax .: +202-22713524<br>
                          </p>
                      </div>
                  </div>
                </div>
              </div>

              <div class=\"location\">
              <div class=\"row\">
              <div class=\"col-md-6 col-xs-12\">
                <div>
                    <form class=\"form-group\" autocomplete=\"off\" method=\"get\">
                      <div class=\"input-field\">
                        <i class=\"fas fa-user-alt\"></i>
                        <input type=\"text\" class=\"form-control\" class=\"validate\" placeholder=\"Name\" required>
                      </div>

                      <div class=\"input-field\">
                        <i class=\"fas fa-envelope\"></i>
                        <input id=\"email\" class=\"form-control\" type=\"email\" class=\"validate\" placeholder=\"Email\" required>
                      </div>
                      
                      <div class=\"input-field\">
                    <i class=\"far fa-comment-dots\"></i>
                        <textarea id=\"textarea\"  class=\"form-control\" placeholder=\"Your Message\" required></textarea>
                      
                      </div>

                      <div class=\"input-wrap\">
                        <button id=\"confirm\" class=\"btn btn-info\" type=\"submit\">Submit</button>
                      </div>

                    </form>
                </div>
              </div>

              <div class=\"col-md-6 col-xs-12\">
                 <div class=\"map\">
                    <a href=\"https://www.google.com/maps/@30.037631,31.351461,4053m/data=!3m1!1e3?hl=en\" target=\"_blank\"><img alt=\"y8\" src=\"";
        // line 76
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null)), "html", null, true);
        echo "/images/map.png\" /></a>
                 </div>
              </div>
            
            </div>
           </div>
          </div>
        </div>
  ";
    }

    public function getTemplateName()
    {
        return "themes/custom/orange/templates/contact/block--views-block--contact-block-1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 76,  99 => 18,  96 => 17,  90 => 85,  88 => 17,  82 => 15,  74 => 13,  72 => 12,  68 => 11,  63 => 10,  61 => 6,  60 => 5,  59 => 3,  56 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("
{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    'clearfix',
  ]
%}
<section{{ attributes.addClass(classes) }}>
  {{ title_prefix }}
  {% if label %}
    <h2{{ title_attributes.addClass('block-title') }}>{{ label }}</h2>
  {% endif %}
  {{ title_suffix }}

  {% block content %}
        <div class=\"contact-us\">
          <div class=\"container\">

              <div class=\"contact-info\">
                <div class=\"row\">
                  <div class=\"col-md-6 col-xs-12\">
                      <div class=\"left\">
                          <h2>Contact Us</h2>
                          <p>
                             <b> Orange Studio</b> <br>
                                  Road 8, Block D, Cairo Free Zone, Nasr City, Cairo, EGYPT.
                          </p>
                      </div>
                  </div>
                  <div class=\"col-md-6 col-xs-12\">
                      <div class=\"right\">
                          <p>
                                      Tel .:  +202-22713519<br>
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                          +202-22713517<br>
                                  Fax .: +202-22713524<br>
                          </p>
                      </div>
                  </div>
                </div>
              </div>

              <div class=\"location\">
              <div class=\"row\">
              <div class=\"col-md-6 col-xs-12\">
                <div>
                    <form class=\"form-group\" autocomplete=\"off\" method=\"get\">
                      <div class=\"input-field\">
                        <i class=\"fas fa-user-alt\"></i>
                        <input type=\"text\" class=\"form-control\" class=\"validate\" placeholder=\"Name\" required>
                      </div>

                      <div class=\"input-field\">
                        <i class=\"fas fa-envelope\"></i>
                        <input id=\"email\" class=\"form-control\" type=\"email\" class=\"validate\" placeholder=\"Email\" required>
                      </div>
                      
                      <div class=\"input-field\">
                    <i class=\"far fa-comment-dots\"></i>
                        <textarea id=\"textarea\"  class=\"form-control\" placeholder=\"Your Message\" required></textarea>
                      
                      </div>

                      <div class=\"input-wrap\">
                        <button id=\"confirm\" class=\"btn btn-info\" type=\"submit\">Submit</button>
                      </div>

                    </form>
                </div>
              </div>

              <div class=\"col-md-6 col-xs-12\">
                 <div class=\"map\">
                    <a href=\"https://www.google.com/maps/@30.037631,31.351461,4053m/data=!3m1!1e3?hl=en\" target=\"_blank\"><img alt=\"y8\" src=\"{{ directory }}/images/map.png\" /></a>
                 </div>
              </div>
            
            </div>
           </div>
          </div>
        </div>
  {% endblock %}
</section>

", "themes/custom/orange/templates/contact/block--views-block--contact-block-1.html.twig", "C:\\xampp\\htdocs\\orange\\web\\themes\\custom\\orange\\templates\\contact\\block--views-block--contact-block-1.html.twig");
    }
}
