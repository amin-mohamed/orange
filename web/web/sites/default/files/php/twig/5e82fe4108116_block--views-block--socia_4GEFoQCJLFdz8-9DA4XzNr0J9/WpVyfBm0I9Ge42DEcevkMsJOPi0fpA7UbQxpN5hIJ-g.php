<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/orange/templates/footer/block--views-block--social-block-1.html.twig */
class __TwigTemplate_0b8a7aae80dc67aa56ae07968dad2b5a189090d19967b661822494d251747c91 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "if" => 11, "block" => 16];
        $filters = ["clean_class" => 4, "escape" => 9];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => "clearfix"];
        // line 9
        echo "<section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
  ";
        // line 11
        if (($context["label"] ?? null)) {
            // line 12
            echo "    <h2";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title_attributes"] ?? null), "addClass", [0 => "block-title"], "method")), "html", null, true);
            echo ">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "</h2>
  ";
        }
        // line 14
        echo "  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

  ";
        // line 16
        $this->displayBlock('content', $context, $blocks);
        // line 38
        echo "</section>

";
    }

    // line 16
    public function block_content($context, array $blocks = [])
    {
        // line 17
        echo "  <div class=\"row\">
<div class=\"col-md-6 col-ms-6 col-xs-12\">
  <div class=\"left\">
    <p>
     All rights reserved 2011 © <span>Orange Studio</span>
    </p>
  </div>
  </div>

  
<div class=\"col-md-6 col-ms-6 col-xs-12\">
  <div class=\"right\">
    <ul class=\"list-unstyled\">
      <li><a href=\"#\" title=\"Facebook\"  target=\"_blank\"><img alt=\"y8\" src=\"";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null)), "html", null, true);
        echo "/images/social/fb.png\" /></a></li>
      <li><a href=\"https://eg.linkedin.com/company/orange-studio\" title=\"Linkedin\"  target=\"_blank\"><img alt=\"y8\" src=\"";
        // line 31
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null)), "html", null, true);
        echo "/images/social/linkedin.png\"/></a></li>
      <li><a href=\"#\"><img alt=\"y8\" src=\"";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null)), "html", null, true);
        echo "/images/social/gmail.png\" title=\"Gmail\"  target=\"_blank\" /></a></li>
      <li><a href=\"https://twitter.com/OrangeStudio_co\" title=\"Twitter\"  target=\"_blank\"><img alt=\"y8\" src=\"";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null)), "html", null, true);
        echo "/images/social/twitter.png\"/></a></li>
    </ul>
  </div>
  </div>
</div>  ";
    }

    public function getTemplateName()
    {
        return "themes/custom/orange/templates/footer/block--views-block--social-block-1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 33,  119 => 32,  115 => 31,  111 => 30,  96 => 17,  93 => 16,  87 => 38,  85 => 16,  79 => 14,  71 => 12,  69 => 11,  65 => 10,  60 => 9,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    'clearfix',
  ]
%}
<section{{ attributes.addClass(classes) }}>
  {{ title_prefix }}
  {% if label %}
    <h2{{ title_attributes.addClass('block-title') }}>{{ label }}</h2>
  {% endif %}
  {{ title_suffix }}

  {% block content %}
  <div class=\"row\">
<div class=\"col-md-6 col-ms-6 col-xs-12\">
  <div class=\"left\">
    <p>
     All rights reserved 2011 © <span>Orange Studio</span>
    </p>
  </div>
  </div>

  
<div class=\"col-md-6 col-ms-6 col-xs-12\">
  <div class=\"right\">
    <ul class=\"list-unstyled\">
      <li><a href=\"#\" title=\"Facebook\"  target=\"_blank\"><img alt=\"y8\" src=\"{{ directory }}/images/social/fb.png\" /></a></li>
      <li><a href=\"https://eg.linkedin.com/company/orange-studio\" title=\"Linkedin\"  target=\"_blank\"><img alt=\"y8\" src=\"{{ directory }}/images/social/linkedin.png\"/></a></li>
      <li><a href=\"#\"><img alt=\"y8\" src=\"{{ directory }}/images/social/gmail.png\" title=\"Gmail\"  target=\"_blank\" /></a></li>
      <li><a href=\"https://twitter.com/OrangeStudio_co\" title=\"Twitter\"  target=\"_blank\"><img alt=\"y8\" src=\"{{ directory }}/images/social/twitter.png\"/></a></li>
    </ul>
  </div>
  </div>
</div>  {% endblock %}
</section>

", "themes/custom/orange/templates/footer/block--views-block--social-block-1.html.twig", "C:\\xampp\\htdocs\\orange\\web\\themes\\custom\\orange\\templates\\footer\\block--views-block--social-block-1.html.twig");
    }
}
