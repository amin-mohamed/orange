<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/orange/page--front.html.twig */
class __TwigTemplate_834c8e60a7a3544af272b93f6c2db5c952f931ffba89e996cbf0e9c9be9eeb75 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'header' => [$this, 'block_header'],
            'slider' => [$this, 'block_slider'],
            'main' => [$this, 'block_main'],
            'about' => [$this, 'block_about'],
            'services' => [$this, 'block_services'],
            'portofolio' => [$this, 'block_portofolio'],
            'contact' => [$this, 'block_contact'],
            'map' => [$this, 'block_map'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "if" => 5, "block" => 6];
        $filters = ["escape" => 22];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
";
        // line 2
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "fluid_container", [])) ? ("container-fluid") : ("container"));
        // line 3
        echo "
";
        // line 5
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 6
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 26
            echo "      ";
        }
        // line 27
        echo "
";
        // line 29
        echo " ";
        if ($this->getAttribute(($context["page"] ?? null), "slider", [])) {
            // line 30
            echo "   ";
            $this->displayBlock('slider', $context, $blocks);
            // line 35
            echo " ";
        }
        // line 38
        $this->displayBlock('main', $context, $blocks);
        // line 77
        echo "


";
        // line 81
        $this->displayBlock('contact', $context, $blocks);
        // line 87
        echo "
";
        // line 89
        $this->displayBlock('map', $context, $blocks);
        // line 95
        echo "
";
        // line 97
        echo "  ";
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 98
            echo "    ";
            $this->displayBlock('footer', $context, $blocks);
            // line 103
            echo "  ";
        }
        // line 105
        echo "

<!--**********loading page***********-->
<div class=\"loading d-flex justify-content align-items\" id=\"load\">
  <div class=\"loader\"></div>
</div>
<!--***********loading page*************-->

<!--**********scroll up***********-->
  <i class=\"fa fa-arrow-circle-up fa-2x\" onclick=\"topFunction()\" id=\"myBtn\"></i>
<!--***********scroll up*************-->

";
    }

    // line 6
    public function block_header($context, array $blocks = [])
    {
        // line 7
        echo "          <nav class=\"navbar navbar-fixed-top\" id=\"nav\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
              <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
              </button>
              <a class=\"navbar-brand\" href=\"#\">
                <img alt=\"Brand\" src=\"themes/custom/orange/images/logo.png\">
              </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
              ";
        // line 22
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
            </div><!-- /.navbar-collapse -->
          </nav>
        ";
    }

    // line 30
    public function block_slider($context, array $blocks = [])
    {
        // line 31
        echo "      <section id=\"slider\">
        ";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "slider", [])), "html", null, true);
        echo "
      </section>
   ";
    }

    // line 38
    public function block_main($context, array $blocks = [])
    {
        // line 39
        echo "  <div role=\"main\" class=\"main-container ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " js-quickedit-main-content\">
    <div class=\"row\">

      ";
        // line 43
        echo "      ";
        $this->displayBlock('about', $context, $blocks);
        // line 48
        echo "      ";
        // line 49
        echo "
      ";
        // line 51
        echo "      ";
        $this->displayBlock('services', $context, $blocks);
        // line 56
        echo "      ";
        // line 57
        echo "
      ";
        // line 59
        echo "      ";
        $this->displayBlock('portofolio', $context, $blocks);
        // line 64
        echo "      ";
        // line 65
        echo "

";
        // line 71
        echo "

    </div>
  </div>
 ";
    }

    // line 43
    public function block_about($context, array $blocks = [])
    {
        // line 44
        echo "        <div class=\"about\" id=\"about\" >
          ";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "about", [])), "html", null, true);
        echo "
        </div>
      ";
    }

    // line 51
    public function block_services($context, array $blocks = [])
    {
        // line 52
        echo "        <section id=\"services\">
          ";
        // line 53
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services", [])), "html", null, true);
        echo "
        </section>
      ";
    }

    // line 59
    public function block_portofolio($context, array $blocks = [])
    {
        // line 60
        echo "        <section id=\"portfolio\">
          ";
        // line 61
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "portofolio", [])), "html", null, true);
        echo "
        </section>
      ";
    }

    // line 81
    public function block_contact($context, array $blocks = [])
    {
        // line 82
        echo "    <section class=\"container-fluid\" id=\"contact\">
      ";
        // line 83
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "contact", [])), "html", null, true);
        echo "
    </section>
";
    }

    // line 89
    public function block_map($context, array $blocks = [])
    {
        // line 90
        echo "  <div class=\"map\" >
    ";
        // line 91
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "map", [])), "html", null, true);
        echo "
  </div>
";
    }

    // line 98
    public function block_footer($context, array $blocks = [])
    {
        // line 99
        echo "      <footer class=\"footer ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\" role=\"contentinfo\">
        ";
        // line 100
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
      </footer>
    ";
    }

    public function getTemplateName()
    {
        return "themes/custom/orange/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 100,  287 => 99,  284 => 98,  277 => 91,  274 => 90,  271 => 89,  264 => 83,  261 => 82,  258 => 81,  251 => 61,  248 => 60,  245 => 59,  238 => 53,  235 => 52,  232 => 51,  225 => 45,  222 => 44,  219 => 43,  211 => 71,  207 => 65,  205 => 64,  202 => 59,  199 => 57,  197 => 56,  194 => 51,  191 => 49,  189 => 48,  186 => 43,  179 => 39,  176 => 38,  169 => 32,  166 => 31,  163 => 30,  155 => 22,  138 => 7,  135 => 6,  119 => 105,  116 => 103,  113 => 98,  110 => 97,  107 => 95,  105 => 89,  102 => 87,  100 => 81,  95 => 77,  93 => 38,  90 => 35,  87 => 30,  84 => 29,  81 => 27,  78 => 26,  75 => 6,  72 => 5,  69 => 3,  67 => 2,  64 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("
{% set container = theme.settings.fluid_container ? 'container-fluid' : 'container' %}

{# header #}
      {% if page.header %}
        {% block header %}
          <nav class=\"navbar navbar-fixed-top\" id=\"nav\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
              <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
              </button>
              <a class=\"navbar-brand\" href=\"#\">
                <img alt=\"Brand\" src=\"themes/custom/orange/images/logo.png\">
              </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
              {{ page.header }}
            </div><!-- /.navbar-collapse -->
          </nav>
        {% endblock %}
      {% endif %}

{#start navbar#}
 {% if page.slider %}
   {% block slider %}
      <section id=\"slider\">
        {{ page.slider }}
      </section>
   {% endblock %}
 {% endif %}
{#end navbar#}
{# Main #}
{% block main %}
  <div role=\"main\" class=\"main-container {{ container }} js-quickedit-main-content\">
    <div class=\"row\">

      {# start about #}
      {% block about %}
        <div class=\"about\" id=\"about\" >
          {{ page.about }}
        </div>
      {% endblock %}
      {# end about  #}

      {# start services block #}
      {% block services %}
        <section id=\"services\">
          {{ page.services }}
        </section>
      {% endblock %}
      {# end services block #}

      {# start portofolio block #}
      {% block portofolio %}
        <section id=\"portfolio\">
          {{ page.portofolio }}
        </section>
      {% endblock %}
      {# end portofolio block #}


{#        {% block content %}#}
{#          <a id=\"main-content\"></a>#}
{#          {{ page.content }}#}
{#        {% endblock %}#}


    </div>
  </div>
 {% endblock %}
{# End main #}



{# start contact #}
{% block contact %}
    <section class=\"container-fluid\" id=\"contact\">
      {{ page.contact }}
    </section>
{% endblock %}
{# end contact  #}

{# start map #}
{% block map %}
  <div class=\"map\" >
    {{ page.map }}
  </div>
{% endblock %}
{# end map  #}

{# start footer #}
  {% if page.footer %}
    {% block footer %}
      <footer class=\"footer {{ container }}\" role=\"contentinfo\">
        {{ page.footer }}
      </footer>
    {% endblock %}
  {% endif %}
{# end footer #}


<!--**********loading page***********-->
<div class=\"loading d-flex justify-content align-items\" id=\"load\">
  <div class=\"loader\"></div>
</div>
<!--***********loading page*************-->

<!--**********scroll up***********-->
  <i class=\"fa fa-arrow-circle-up fa-2x\" onclick=\"topFunction()\" id=\"myBtn\"></i>
<!--***********scroll up*************-->

", "themes/custom/orange/page--front.html.twig", "C:\\xampp\\htdocs\\orange\\web\\themes\\custom\\orange\\page--front.html.twig");
    }
}
