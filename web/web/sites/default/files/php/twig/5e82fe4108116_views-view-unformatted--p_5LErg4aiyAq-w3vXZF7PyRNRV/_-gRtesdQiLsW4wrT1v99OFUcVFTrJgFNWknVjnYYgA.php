<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/orange/templates/portofolio/views-view-unformatted--portofolio--block_1.html.twig */
class __TwigTemplate_3b57bd3d7efb9768a9748eea5daaeac1b4b072c0c1722d58a65d25a8b4c708c7 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["for" => 12, "set" => 14];
        $filters = ["escape" => 18];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['for', 'set'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
  <p>
   <span class=\"span_header\">Orange Studio</span>
  believes in long term business relationship and client growth and that is why after-sale support is a key factor in our business approach.
Once an Orange Studio client is always an Orange Studio client.
  </p>
<br>
<br>
<br>
<div class=\"owl-carousel owl-theme\">

    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 13
            echo "    ";
            // line 14
            $context["row_classes"] = [0 => ((            // line 15
($context["default_row_class"] ?? null)) ? ("views-row") : (""))];
            // line 18
            echo "    <div";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["row"], "attributes", []), "addClass", [0 => ($context["row_classes"] ?? null)], "method")), "html", null, true);
            echo ">";
            // line 19
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["row"], "content", [])), "html", null, true);
            // line 20
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
</div>

";
    }

    public function getTemplateName()
    {
        return "themes/custom/orange/templates/portofolio/views-view-unformatted--portofolio--block_1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 22,  83 => 20,  81 => 19,  77 => 18,  75 => 15,  74 => 14,  72 => 13,  68 => 12,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("
  <p>
   <span class=\"span_header\">Orange Studio</span>
  believes in long term business relationship and client growth and that is why after-sale support is a key factor in our business approach.
Once an Orange Studio client is always an Orange Studio client.
  </p>
<br>
<br>
<br>
<div class=\"owl-carousel owl-theme\">

    {% for row in rows %}
    {%
        set row_classes = [
        default_row_class ? 'views-row',
        ]
    %}
    <div{{ row.attributes.addClass(row_classes) }}>
        {{- row.content -}}
    </div>
    {% endfor %}

</div>

", "themes/custom/orange/templates/portofolio/views-view-unformatted--portofolio--block_1.html.twig", "C:\\xampp\\htdocs\\orange\\web\\themes\\custom\\orange\\templates\\portofolio\\views-view-unformatted--portofolio--block_1.html.twig");
    }
}
