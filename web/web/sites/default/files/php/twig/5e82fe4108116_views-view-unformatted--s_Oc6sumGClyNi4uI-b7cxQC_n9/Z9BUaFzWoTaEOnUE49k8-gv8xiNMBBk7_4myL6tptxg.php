<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/orange/templates/servicess/views-view-unformatted--services.html.twig */
class __TwigTemplate_637c789ae5a42776a9bda3dcfc4658c114791a44863ea19bb1fa759fd9d7e49a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1, "for" => 14, "set" => 16];
        $filters = ["escape" => 2];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for', 'set'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if (($context["title"] ?? null)) {
            // line 2
            echo "  <h3>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
            echo "</h3>
";
        }
        // line 4
        echo "
<p class=\"description\">
  <span class=\"span_header\">Orange Studio</span>
    offers a wide range of web development services which are tailored to each clients individual needs and requirements. We specialize in offering complete end to end solutions in which we take care of the project from start to finish and have a range of maintenance contracts available. Orange Studio is also able to offer Search Engine Optimization and Internet marketing services for your completed website.
    We select the right technologies and tools that provide the most flexible and powerful solutions for the situation. We are inspired by the best of today's technology and can apply it to your problems.
</p>



<div class=\"row\">
    ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 15
            echo "    ";
            // line 16
            $context["row_classes"] = [0 => ((            // line 17
($context["default_row_class"] ?? null)) ? ("views-row") : ("")), 1 => "col-lg-3 col-md-6 col-sm-6 col-xs-12"];
            // line 20
            echo "    <div";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["row"], "attributes", []), "addClass", [0 => ($context["row_classes"] ?? null)], "method")), "html", null, true);
            echo ">";
            // line 21
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["row"], "content", [])), "html", null, true);
            // line 22
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/orange/templates/servicess/views-view-unformatted--services.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 24,  90 => 22,  88 => 21,  84 => 20,  82 => 17,  81 => 16,  79 => 15,  75 => 14,  63 => 4,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if title %}
  <h3>{{ title }}</h3>
{% endif %}

<p class=\"description\">
  <span class=\"span_header\">Orange Studio</span>
    offers a wide range of web development services which are tailored to each clients individual needs and requirements. We specialize in offering complete end to end solutions in which we take care of the project from start to finish and have a range of maintenance contracts available. Orange Studio is also able to offer Search Engine Optimization and Internet marketing services for your completed website.
    We select the right technologies and tools that provide the most flexible and powerful solutions for the situation. We are inspired by the best of today's technology and can apply it to your problems.
</p>



<div class=\"row\">
    {% for row in rows %}
    {%
        set row_classes = [
        default_row_class ? 'views-row', 'col-lg-3 col-md-6 col-sm-6 col-xs-12'
        ]
    %}
    <div{{ row.attributes.addClass(row_classes) }}>
        {{- row.content -}}
    </div>
    {% endfor %}
</div>
", "themes/custom/orange/templates/servicess/views-view-unformatted--services.html.twig", "C:\\xampp\\htdocs\\orange\\web\\themes\\custom\\orange\\templates\\servicess\\views-view-unformatted--services.html.twig");
    }
}
